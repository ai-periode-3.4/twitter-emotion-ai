﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace TwitterEmotionAI_App
{
    /**
     * Acts as a service for retrieving Twitter Data
     */
    public class TwitterService
    {
        private readonly HttpClient _client;

        // Twitter API endpoint for retrieving tweets
        private const string RequestUri =
            "https://api.twitter.com/2/users/13298072/tweets?tweet.fields=created_at,public_metrics&expansions=author_id&user.fields=created_at&max_results=100";

        // Provide your own Twitter API token.
        private const string TwitterApiToken = "";

        public TwitterService()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TwitterApiToken);
        }

        /**
         * GetTweets returns a list of TweetData, async
         */
        public async Task<List<TweetData>> GetTweets()
        {
            // Perform get request to API endpoint
            HttpResponseMessage response = await _client.GetAsync(RequestUri);
            response.EnsureSuccessStatusCode();

            // Response's body to string
            string responseBody = await response.Content.ReadAsStringAsync();

            // ResponseBody to JSON object
            JObject json = JObject.Parse(responseBody);

            // Access data array
            JArray dataArray = (JArray)json["data"];

            if (dataArray.Count == 0)
            {
                return null;
            }

            List<TweetData> tweetList = new List<TweetData>();

            // Iterate through the data array
            foreach (var item in dataArray)
            {
                var tweetObject = (JObject)item;

                var id = tweetObject.GetValue("id").ToString();
                var text = tweetObject.GetValue("text").ToString();
                var createdAt = tweetObject.GetValue("created_at").ToObject<DateTime>();

                var metricsObject = (JObject)tweetObject["public_metrics"];

                var reTweetCount = metricsObject.GetValue("retweet_count").ToObject<int>();
                var replyCount = metricsObject.GetValue("reply_count").ToObject<int>();
                var likeCount = metricsObject.GetValue("like_count").ToObject<int>();
                var quoteCount = metricsObject.GetValue("quote_count").ToObject<int>();

                var newTweetData = new TweetData
                {
                    Id = id,
                    Text = text,
                    CreatedAt = createdAt,
                    ReTweetCount = reTweetCount,
                    ReplyCount = replyCount,
                    LikeCount = likeCount,
                    QuoteCount = quoteCount
                };

                tweetList.Add(newTweetData);
            }

            return tweetList;
        }
    }
}
