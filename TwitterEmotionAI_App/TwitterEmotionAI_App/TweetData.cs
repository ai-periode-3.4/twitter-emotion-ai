﻿using System;

namespace TwitterEmotionAI_App
{
    /**
     * Tweet data, used for mapping tweet data of Twitter API
     */
    public class TweetData
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }

        public int ReTweetCount { get; set; }

        public int ReplyCount { get; set; }

        public int LikeCount { get; set; }

        public int QuoteCount { get; set; }
    }
}
