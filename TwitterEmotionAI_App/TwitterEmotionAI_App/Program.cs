﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.ML;
using TwitterEmotionAI_ProjectML.Model;

namespace TwitterEmotionAI_App
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await PredictTweetsSentiment();
            // await PredictTweetsSentimentWithGeneratedModel();
        }

        static async Task PredictTweetsSentimentWithGeneratedModel()
        {
            // Create twitter service
            var service = new TwitterService();

            // Get tweets
            var results = await service.GetTweets();

            var predictions = new ArrayList();

            // Iterate
            foreach (var data in results)
            {
                // Create prediction
                var predictionResult = ConsumeModel.Predict(new ModelInput
                {
                    Id = float.Parse(data.Id),
                    Tweet = data.Text
                });

                var sentimentResult = new SentimentResult
                {
                    ModelOutput = predictionResult,
                    TweetData = data
                };

                predictions.Add(sentimentResult);
            }

            // Order list by score
            var sortedPredictionsList =
                predictions.Cast<SentimentResult>().OrderBy(p => p.ModelOutput.Score);

            // Iterate through sorted predictions list
            foreach (SentimentResult data in sortedPredictionsList)
            {
                var tweet = data.TweetData;
                var predictionResult = data.ModelOutput;

                Console.WriteLine("----------------------------------------------------------------------------\n\n");
                Console.WriteLine($"Id: {tweet.Id}");
                Console.WriteLine($"Text: {tweet.Text}");
                Console.WriteLine($"CreatedAt: {tweet.CreatedAt}");
                Console.WriteLine(
                    $"\n\nPredicted Label value {predictionResult.Prediction} \nPredicted Label scores: [{String.Join(",", predictionResult.Score)}]\n\n");
            }

        }



        static async Task PredictTweetsSentiment()
        {
            // Create twitter service
            var service = new TwitterService();

            // Get tweets
            var tweetsList = await service.GetTweets();

            // Create MLContext
            var context = new MLContext();

            // Get training data
            // Add your own .csv file
            var data = context.Data.LoadFromTextFile<TweetSentimentData>(Directory.GetCurrentDirectory() + "/data/train.csv", hasHeader: true,
                separatorChar: ',');

            // Create pipeline for training data
            var pipeline = context.Transforms.Expression("Label", "(x) => x == 1 ? true : false", "Label")
                .Append(context.Transforms.Text.FeaturizeText("Features", nameof(TweetSentimentData.Tweet)))
                .Append(context.BinaryClassification.Trainers
                    .SdcaLogisticRegression()); // Use sdca logistic regression

            // Create ai model
            var model = pipeline.Fit(data);

            // Create engine for prediction
            var predictionEngine =
                context.Model.CreatePredictionEngine<TweetSentimentData, SentimentPrediction>(model);

            var predictions = new ArrayList();

            foreach (var tweet in tweetsList)
            {
                var prediction = predictionEngine.Predict(new TweetSentimentData { Tweet = tweet.Text });

                var sentimentResult = new SentimentResult { TweetData = tweet, SentimentPrediction = prediction };

                predictions.Add(sentimentResult);
            }

            // Order predictions list by probability (associates with value of score)
            var sortedPredictionsList =
                predictions.Cast<SentimentResult>().OrderBy(p => p.SentimentPrediction.Probability);

            // Iterate 
            foreach (var sentimentResult in sortedPredictionsList)
            {
                var tweet = sentimentResult.TweetData;
                var prediction = sentimentResult.SentimentPrediction;

                Console.WriteLine(
                    "----------------------------------------------------------------------------\n\n");
                Console.WriteLine($"Id: {tweet.Id}");
                Console.WriteLine($"Text: {tweet.Text}");
                Console.WriteLine($"CreatedAt: {tweet.CreatedAt}");
                Console.WriteLine(
                    $"Predicted Label value {prediction.Prediction} \nPredicted Label scores: [{String.Join(",", prediction.Score)}]");

                switch (prediction.Probability)
                {
                    case float p when p < .5:
                        Console.WriteLine(
                            $"\n\nTweet sentiment is negative with probability of {prediction.Probability}\n\n");
                        break;
                    case float p when p >= .5 && p <= .7:
                        Console.WriteLine(
                            $"\n\nTweet sentiment is neutral with probability of {prediction.Probability}\n\n");
                        break;
                    case float p when p > .7:
                        Console.WriteLine(
                            $"\n\nTweet sentiment is positive with probability of {prediction.Probability}\n\n");
                        break;
                    default:
                        break;

                }
            }
        }
    }
}
