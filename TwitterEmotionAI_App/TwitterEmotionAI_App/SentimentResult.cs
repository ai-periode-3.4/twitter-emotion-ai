﻿using System;
using System.Collections.Generic;
using System.Text;
using TwitterEmotionAI_ProjectML.Model;

namespace TwitterEmotionAI_App
{
    public class SentimentResult
    {
        public TweetData TweetData { get; set; }
        public ModelOutput ModelOutput { get; set; }
        public SentimentPrediction SentimentPrediction { get; set; }
    }
}
