﻿using Microsoft.ML.Data;

namespace TwitterEmotionAI_App
{
    /**
     * Sentiment model for data from train.csv
     */
    public class TweetSentimentData
    {
        [LoadColumn(0)]
        public int Id { get; set; }

        [LoadColumn(1)]
        public int Label { get; set; }

        [LoadColumn(2)]
        public string Tweet { get; set; }
    }
}
