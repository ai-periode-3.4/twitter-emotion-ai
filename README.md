# Twitter Emotion Sentiment Analysis AI

## Description
This project focusses on scoring a Tweet on their emotional value based on the Tweet's sentiment.

## Twitter API
Usage of Twitter API is applied in this project.
Please provide your own Twitter API key in TwitterService.cs .

[Twitter API](https://developer.twitter.com/en/products/twitter-api)

## Training data
The data that has been used for training the AI is from Ali Toosi, available on Kaggle.
You may need to provide the full path of <i>train.csv</i> in the <i>Program.cs</i> of <i>TwitterEmotionAI_App</i> if the file doesn't load.

[Kaggle - Twitter Sentiment Analysis](https://www.kaggle.com/arkhoshghalb/twitter-sentiment-analysis-hatred-speech?select=train.csv)

## Methods
In this project, scoring on the sentiment value of tweets have been done in 2 ways:


### <u>Created own prediction</u> 
The Program.cs of TwitterEmotionAI_App contains the method <i>PredictTweetsSentiment()</i> where a MLContext has been created with a pipeline that loads the data of train.csv and applies <i>Sdca Logistic Regression</i> algorithm in order to create the prediction engine.

&nbsp;

### <u>Using <span>ML.NET</span> Model Builder - Text classification</u>
`ML.NET` is a cross-platform machine learning platform where custom ML models can be created. For this project. `ML.NET` generated the project TwitterEmotionAI_ProjectML.Model where the ML model is stored with the most optimal algorithm. This allows to create predictions without training the prediction engine over and over.

#### <strong>Training results</strong>
<img src="./img/training_results.jpg" width="500" alt="training results" />

&nbsp;

#### <strong>Training results - ML models</strong>
<img src="./img/training_results_models.jpg" width="500" alt="training results" />
